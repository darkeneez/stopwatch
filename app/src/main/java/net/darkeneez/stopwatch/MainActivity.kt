package net.darkeneez.stopwatch

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), Runnable {
    private val labelStop = "Стоп"
    private val labelStart = "Старт"
    private val labelZero = "00:00:00:00"

    private var startTime: Long = 0
    private var timeLeft: Long = 0

    private var isStart: Boolean = false
    private var handler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        startButton.setOnClickListener {
            if (!isStart)
                startStopWatch()
            else
                stopStopwatch()
        }

        resetButton.setOnClickListener { resetStopwatch() }
    }

    private fun startStopWatch() {
        resetButton.isEnabled = false
        startButton.text = labelStop


        isStart = true
        startTime = System.currentTimeMillis()

        startTimer()
    }

    private fun stopStopwatch() {
        isStart = false

        timeLeft += System.currentTimeMillis() - startTime

        resetButton.isEnabled = true
        startButton.text = labelStart
    }

    private fun resetStopwatch() {
        startTime = 0
        timeLeft = 0

        stopWatchLabel.text = labelZero
    }

    private fun startTimer() {
        handler = Handler()
        handler?.postDelayed(this, 10)
    }

    private fun updateStopwatch() {
        stopWatchLabel.text = getTime(System.currentTimeMillis() - startTime + timeLeft)
    }

    override fun run() {
        if (isStart) {
            updateStopwatch()
            startTimer()
        }
    }

    private fun getTime(time: Long): String {
        var ms = time

        val hours = ms / 3600000
        ms %= 3600000
        val minutes = ms / 60000
        ms %= 60000
        val seconds = ms / 1000
        ms %= 1000

        return String.format("%02d:%02d:%02d:%03d", hours, minutes, seconds, ms)
    }
}
